import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { createAppContainer } from "react-navigation";
import HomePage from "../screens/HomePage";
import SettingsPage from "../screens/SettingsPage";
import DetailsPage from "../screens/DetailsPage"
import Icon from "react-native-vector-icons/FontAwesome";
import Icon2 from "react-native-vector-icons/Octicons";
import { createStackNavigator } from "react-navigation-stack";
import React from 'react';



const DetailsNavigator = createStackNavigator(
    {
        Home: {screen: HomePage},
        Details: {screen: DetailsPage}
    },

    {
        initialRouteName: 'Home',
        defaultNavigationOptions:{
            headerStyle : {backgroundColor:'#f411e'},
            headerTintColor: '#fff',
            headerTitleStyle:{fontWeight:'bold'}
        }
    }
);


const tabNavigator = createMaterialBottomTabNavigator({
        Home: {
            screen: HomePage,
            navigationOptions:{
                tabBarLabel: 'Accueil',
                tabBarIcon: ({tintColor}) => (
                    <Icon color={tintColor} size={25} name={'newspaper-o'} />
                ),
                barStyle: { backgroundColor: 'silver' }
            }
        },

        Settings: {
            screen: SettingsPage,
            navigationOptions:{
                tabBarLabel: 'Parametre',
                tabBarIcon: ({tintColor}) => (
                    <Icon2 color={tintColor} size={25} name={'settings'} />
                ),
                barStyle: { backgroundColor: 'grey' }
            }
        }
    },

    {
        initialRouteName: 'Home'
    }
);

export default createAppContainer(tabNavigator);