import axios from 'axios';

const key = '9e4b087cb6604d7f85e61b4fe4dd9aae';
const url = `https://newsapi.org/v2/top-headlines?country=fr&apiKey=${key}`;

class NewsService{

    getNews(){
        return axios.get(`${url}`);
    }
}

export default NewsService;