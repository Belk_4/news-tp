import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, StyleSheet , Button} from 'react-native';
import axios from 'axios';


class Search extends Component {

    state = {
        recherche : ''
     }

     handleRecherche = (text) => {
        this.setState({ recherche : text })
     }


     search = (recherche) => {
        const key = '9e4b087cb6604d7f85e61b4fe4dd9aae';
        const url = `https://newsapi.org/v2/top-headlines?q=${recherche}country=fr&apiKey=${key}`;
        return axios.get(`${url}`);
     }

     render() {
        return (
           <View >
              <TextInput 
                 underlineColorAndroid = "transparent"
                 placeholder = "Recherche"
                 placeholderTextColor = "#9a73ef"
                 autoCapitalize = "none"
                 onChangeText = {this.handleRecherche}
                 style = {{flex: 1, justifyContent: 'center', alignItems: 'center' }}
                 />



              <TouchableOpacity

                 onPress = {
                    () => this.search(this.state.recherche)
                 }>
                 <Text > Submit </Text>

                 <Button title="Voir résultat" onPress={() =>{this.search}}/>


              </TouchableOpacity>
           </View>
        )
     }
  }


export default Search;