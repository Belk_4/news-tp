import React, { Component } from 'react';
import {Text, View} from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import PropTypes from 'prop-types';

class Loading extends Component {

    static PropTypes = {
        displayColor: PropTypes.string.isRequired
    }

    state = {}

    render() {
        return (
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}} >
                <ActivityIndicator size='large' color={this.props.displayColor} />
                <Text style ={{color: this.props.displayColor}}>Chargement...</Text>
                {this.props.children}
            </View>
        );
    }
}

export default Loading;