import React, { Component } from 'react';
import {Text, View} from 'react-native';
import PropTypes from 'prop-types';

class ItemNews extends Component {

    static propTypes = {
        Info: PropTypes.any.isRequired
    }

    state = {}

    render(){
        return (   
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text>{this.props.Info.titel}</Text>
                <Image style={{width:100 , height:100}} source = {{uri : `${this.props.Info.urlToImage}`}}/>
            </View>
        );
    }
}

export default ItemNews;